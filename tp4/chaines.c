#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "fonctions.h"

int main(int argc, char const *argv[])
{
	int is_s = 0;
	int is_m = 0;

	if (argc == 1)
	{
		printf("Mauvaise utilisations\n");
		return 1;
	}
	for (int i = 1; i < argc; ++i){
		
		if (argv[i][0] == '-'){

			if (strlen(argv[i]) == 2){

				if (argv[i][1] == 's')
					is_s = 1;

				if (argv[i][1] == 'm')
					is_m = 1;

			}else if(strlen(argv[i]) > 2 && is_m == 0){
				int j = 1;
				while(argv[i][j] != '\0'){
					//printf("%c\n", argv[i][j]);
					if (argv[i][j] != 's' && argv[i][j] != 'm'){
						printf("Mauvaise utilisations\n");
						return 1;
					}
					j++;
				}
			}else{
				printf("Mauvaise utilisations\n");
				return 1;
			}

		}else if(is_m == 0){
			printf("Mauvaise utilisations\n");
			return 1;
		}
		
	}

	if(is_m == 1 && is_s == 0){
		if (argc == 2){
			printf("Mauvaise utilisations\n");
			return 1;
		}
		for (int i = strlen(argv[2])-1; i >=0; --i)
		{ 
			printf("%c", argv[2][i]);
		}
		printf("\n");
	}else if(is_s == 1 && is_m == 0){
		char *text = saisie();
		for (size_t i = 0; i < strlen(text); ++i)
		{
			printf("%c", text[i]);
		}
		printf("\n");
	}else{
		char *text = saisie();
		for (int i = strlen(text)-1; i >=0; --i)
		{
			printf("%c", text[i]);
		}
		printf("\n");
	}

	return 0;
}