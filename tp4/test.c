#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include "fonctions.h"
#include "munit.h"

#define UNUSED(x) ((void)(x))
#define ALLOC_PADDING_SIZE 32
#define ALLOC_PADDING_VALUE 0xCD

void test_free(void *ptr);
size_t block_size(void *ptr);

MunitResult test_miroir(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  char *t = miroir("Titi");
  munit_assert_string_equal(t, "itiT");
  munit_assert_size(block_size(t), ==, strlen(t)+1);
  munit_assert_uchar(t[4], ==, '\0');
  munit_assert_uchar(t[5], ==, ALLOC_PADDING_VALUE);
  test_free(t);


  t = miroir("");
  munit_assert_string_equal(t, "");
  munit_assert_size(block_size(t), ==, strlen(t)+1);
  munit_assert_uchar(t[0], ==, '\0');
  munit_assert_uchar(t[1], ==, ALLOC_PADDING_VALUE);
  test_free(t);


  const char *orig = "Chaine de test";
  char *m = miroir(orig);
  munit_assert_size(block_size(m), ==, strlen(m)+1);
  munit_assert_ptr_not_equal(orig, m);
  test_free(m);
  return MUNIT_OK;
}



#define WRITE_STR(d,s) write((d), (s), strlen((s)))
#define BIG_SIZE 1000000
MunitResult test_saisie(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  // use a pipe and redirect stdin to send test data to function
  int p[2];
  munit_assert_int(pipe(p), !=, -1);
  int pid = fork();
  if (pid == 0) {
    close(p[1]);
    close(0);
    dup(p[0]);
    close(p[0]);
    // saisie can now be called

    char *s = saisie();
    munit_assert_string_equal(s, "Test");
    munit_assert_size(block_size(s), ==, strlen(s)+1);
    test_free(s);

    s = saisie();
    munit_assert_string_equal(s, "facile");
    munit_assert_size(block_size(s), ==, strlen(s)+1);
    test_free(s);

    s = saisie();
    munit_assert_string_equal(s, "Testavec");
    munit_assert_size(block_size(s), ==, strlen(s)+1);
    test_free(s);

    s = saisie();
    munit_assert_string_equal(s, "tabulation");
    munit_assert_size(block_size(s), ==, strlen(s)+1);
    test_free(s);

    s = saisie();
    for (size_t i = 0 ; i < BIG_SIZE; ++i) {
      munit_assert_char(s[i], ==, 'Z');
    }
    munit_assert_size(block_size(s), ==, strlen(s)+1);
    test_free(s);
    
    exit(MUNIT_OK);
  }
  close(p[0]);
  WRITE_STR(p[1], "Test facile\n");
  WRITE_STR(p[1], "Testavec\ttabulation\n");
  unsigned char *big_buff = malloc(BIG_SIZE/10);
  memset(big_buff, 'Z', BIG_SIZE/10);
  for (int i = 0 ; i < 10 ; ++i)
    munit_assert_int(write(p[1], big_buff, BIG_SIZE/10), != , -1);
  free(big_buff);
  close(p[1]);
  int status;
  munit_assert_int(wait(&status), !=, -1);
  munit_assert(WIFEXITED(status));
  return WEXITSTATUS(status);
}


MunitTest tests[] = {
    {"/miroir", test_miroir, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/saisie", test_saisie, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    /* Mark the end of the array with an entry where the test
     * function is NULL */
    {NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}};

static const MunitSuite suite = {
    "/saisie_mirroir",       /* name */
    tests,          /* tests */
    NULL,                   /* suites */
    3,                    /* iterations */
    MUNIT_SUITE_OPTION_NONE /* options */
};

int main(int argc, char **argv) {

  int ret = munit_suite_main(&suite, NULL, argc, argv);
  return ret;
}


typedef struct {
  size_t alloc_size;
  unsigned char *ptr;
} alloc_block;


#define ALLOC_TOTAL_SIZE(size)                                                 \
  ((size) + ALLOC_PADDING_SIZE * 2 + sizeof(alloc_block))
#define BLOCK_FROM_PTR(ptr) ((alloc_block*)(((unsigned char*)(ptr)) - ALLOC_PADDING_SIZE - sizeof(alloc_block)))


// A test malloc that stores information on the allocated block and
// adds padding before and after allocated block, so that outside
// bounds writes can be detected at free
void *test_malloc(size_t size) {
  alloc_block *block = malloc(ALLOC_TOTAL_SIZE(size));
  if (block == NULL)
    return NULL;
  block->alloc_size = size;
  block->ptr = ((unsigned char*)block) + sizeof(alloc_block);
  memset(block->ptr, ALLOC_PADDING_VALUE, size + ALLOC_PADDING_SIZE*2);
  return block->ptr + ALLOC_PADDING_SIZE;
}

void test_free(void *ptr) {
  alloc_block *block = BLOCK_FROM_PTR(ptr);

  for (size_t i = 0 ; i < ALLOC_PADDING_SIZE ; ++i) {
    munit_assert_uchar(block->ptr[i], ==, ALLOC_PADDING_VALUE);
  }
  for (size_t i = block->alloc_size + ALLOC_PADDING_SIZE; i < block->alloc_size + ALLOC_PADDING_SIZE*2 ; ++i) {
    munit_assert_uchar(block->ptr[i], ==, ALLOC_PADDING_VALUE);
  }
  // Clear freed space. This will ensure that when calling naive realloc, the previously allocated block is unusable
  memset(block, 0xAB, ALLOC_TOTAL_SIZE(block->alloc_size));
  free(block);
}


size_t block_size(void *ptr) {
  return BLOCK_FROM_PTR(ptr)->alloc_size;
}

#define min(a,b) ((a) < (b) ? (a) : (b))

// A realloc implementation for tests
// This is totally naive and ALWAYS call malloc
// This will ensure that returned value will never be ptr
// so that the old block can not be used
void *test_realloc(void *ptr, size_t size) {
  unsigned char *new_ptr = test_malloc(size);
  if (new_ptr == NULL)
    return NULL;
  alloc_block *block = BLOCK_FROM_PTR(ptr);
  // copy old data
  memcpy(new_ptr, ptr, min(size, block->alloc_size));
  // free old ptr
  test_free(ptr);
  // return new ptr
  return new_ptr;
}