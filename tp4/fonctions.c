#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "test.h" 
#include <ctype.h>

char * miroir(const char *s){
	char* chaine_miroir = malloc(strlen(s) * sizeof(char)+1);
	if (chaine_miroir == NULL)
	{
		return NULL;
	}

	int i = strlen(s)-1;
	int j = 0;
	for (; i >= 0; --i)
	{
		*(chaine_miroir+j) = *(s + i);
		//printf("%s\n", chaine_miroir);
		j++;
	}
	*(chaine_miroir+(j)) = '\0';
	return chaine_miroir;
}

char * saisie(){
	int nbcarac = 0;
	int max = 200;
	char * chaine = malloc(max);
	char c = getchar();

	while(c != EOF && c!= '\n' && isspace(c) == 0){
		nbcarac++;
		if (nbcarac % max == 0)
			chaine = realloc(chaine, max + nbcarac);
		//*(chaine + nbcarac-1) = c;
		chaine[nbcarac - 1] = c;
		c = getchar();
	}
	
	if (chaine == NULL)
	{
		free(chaine);
		return NULL;
	}
	*(chaine + nbcarac) = '\0';
	chaine = realloc(chaine, nbcarac+1);
	return chaine;
}
