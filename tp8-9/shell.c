#include "ligne_commande.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

void affiche_prompt(){
    const char * name = getenv("USERNAME");
    char host[256]; 
    char cwd[256];
    getcwd(cwd, sizeof(cwd));
    gethostname(host, sizeof(host)); 
    printf("%s@%s %s$ ",name,host,cwd);
    fflush(stdout);
}

int lance_commande(int in, int out, char *com, char **argv){
    pid_t pid;
    int status;
    pid = fork();

     if (pid == 0){
        if (in != 0){
           close(0);
           dup(in);
        }
        if (out != 1){
            close(1);
            dup(out);
        }
        status = execvp(com, argv);
        perror(com);
        exit(status);
    }else{
        if(in != 0)
            close(in);
        if(out != 1)
            close(out);
    }
    
	return pid;
}


void execute_ligne_commande(){
    int flag, nb, i;
    int in, out;
    int fd[2] = {0, 1};
    int sin = dup(0);
    int sout = dup(1);
    int status;
    char ***cmd = ligne_commande(&flag, &nb);

    if(flag == -1){
        free(cmd);
        return;
    }

  for (i = 0; i < nb; i++) 
    {
        //Permet de quitter plus proprement le terminal
        if (strcmp(cmd[0][0],"qu") == 0)
        {
            exit(1);
        }
      in = sin;
      out = sout;

      if (i != 0)
	  in = fd[0];
     
      if (nb > 1 && i != nb - 1) {
	    pipe(fd);
      }

      if (i != nb - 1)
	    out = fd[1];

      lance_commande(in, out, cmd[i][0], cmd[i]);
    }

    if (flag == 0) {
        int p;
        while((p = waitpid(-1,&status,WNOHANG)) >= 0){
            printf("attente\n");
            sleep(2);
        }
    }else
    {
        int p;
        while((p = waitpid(-1,&status,WNOHANG)) > 0){
            if(WIFEXITED(status)){
                    printf("Père: le fils s'est terminé correctement avec le code %d pid : %d\n",
                            WEXITSTATUS(status), p);
                    fflush(stdout);
            }else {
                    printf("Père: le fils s'est terminé anormalement par le signal %d pid : %d\n",
                        WTERMSIG(status),p);
            }
        }
    }

    free(cmd);
}
