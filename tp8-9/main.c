#include "shell.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{ 
    while (1)
    {
        sleep(1);
        affiche_prompt();
        execute_ligne_commande();     
    }

    return 0;
}
