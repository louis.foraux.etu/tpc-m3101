#ifndef __LIGNE_COMMANDE_H__
#define __LIGNE_COMMANDE_H__

/** Lit une ligne depuis l'entrée standard et la retourne sous la
 * forme d'un tableau de tableaux de chaînes de caractères.  La ligne
 * est découpée tout d'abord en utilisant le caratère '|’ comme
 * séparateur, puis chaque partie est à nouveau découpée par mot.
 * 
 * Il s'agit donc d'un tableau de commande à exécuter, chaque
 * commander étant donnée au format d'un tableau de type argv
 * (utilisable donc directement avec execv*)
 
 * @param[out] flag un pointeur vers un entier dans lequel la fonction
 * retournera 1 si la ligne de commande est valide et est terminée par
 * le caractère '&', 0 si la ligne est valide mais ne se termine pas
 * par '&' et -1 si une erreur est survenue.
 *
 * @param[out] nb un pointeur vers un entier dans lequel la fonction
 * retournera le nombre de commandes lues. Attention, si une erreur se
 * produit ou si aucune commande n'est tapée, la variable pointée par
 * nb ne sera pas modifiée et la fonction retournera NULL
 *
 * @return Un tableau de commande, chaque commande étant un tableau de
 * chaînes de caractères au format type 'argv'. Attention, la fonction
 * peut, en cas d'erreur ou si aucune commande n'a été saisie,
 * retourner NULL. Le tableau est construit par allocation dynamique,
 * il faudra utiliser la fonction libere pour libérer la mémoire allouée.
 */

char ***ligne_commande (int *flag, int *nb);


/** Analyse une ligne de commande.
    @sa ligne_commande
    
    @param[in] ligne la ligne à analyser. Attention, la fonction utilisant strtok, la ligne sera modifiée
    @param[out] flag @sa ligne_commande
    @param[out] flag @sa ligne_commande
    @return @sa ligne_commande
*/
char ***analyse_ligne_commande(char *ligne, int *flag, int *nb);

/** Affiche une ligne de commande précédemment retournée par ligne_commande ou analyse_ligne_commande */
void affiche (char ***t);

/** Libere une ligne de commande précédemment retournée par ligne_commande ou analyse_ligne_commande */
void libere (char ***t);

#endif
