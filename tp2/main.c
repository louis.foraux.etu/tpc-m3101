#include <stdio.h>
#include "rat.h"
#include "entier.h"

#define TAILLE 100

int main(void)
{
	printf("EXO 1\n");
	int tab_dest_copie[10];
	int tab10[10];
	for (int i = 0; i < 10; ++i)
	{
		tab10[i] = i;
	}

	printf("QUESTION 1 : Affichage\n");
	afficher(tab10,10);

	printf("QUESTION 2 : Somme\n");
	printf("%d\n", somme(tab10,10));

	printf("QUESTION 3 : Copie + affichage\n");
	copie_dans(tab_dest_copie, tab10, 10);

	afficher(tab_dest_copie,10);

	printf("QUESTION 4 : Ajout dans un tableau\n");

	int tab_ajout[TAILLE];
	tab_ajout[0] = 12;
	tab_ajout[1] = 56;

	ajoute_apres(tab_ajout,2, tab10,10);

	afficher(tab_ajout,12);
	printf("\n\n\nEXO 2\n\n\n");
	printf("QUESTION 1 : Produit\n");
	struct rat r1,r2,r3,r4;
	r1.num = 1;r1.den = 2;
	r2.num = 3;r2.den = 2;
	r3.num = 1;r3.den = 8;
	r4.num = 1;r4.den = 4;

	struct rat r_result_produit = rat_produit(r1,r2);
	printf("num : %d den : %d\n", r_result_produit.num, r_result_produit.den);
	
	printf("QUESTION 2 : Somme\n");
	struct rat r_result_somme = rat_somme(r3,r4);
	printf("num : %d den : %d\n", r_result_somme.num, r_result_somme.den);
	

	printf("QUESTION 3 : détection de min\n");
	struct rat list[4]; list[0] = r1;list[1] = r2;list[2] = r3;list[3] = r4;
	struct rat r_result_min = rat_plus_petit(list);

	printf("num : %d den : %d\n", r_result_min.num, r_result_min.den);
	return 0;
}