#include <stdio.h>

struct rat{
	int den;
	int num;
};

struct rat rat_produit(struct rat n1,struct rat n2){
	struct rat result;

	result.den = n1.den * n2.den;
	result.num = n1.num * n2.num;

	return result;
}

struct rat rat_somme(struct rat n1,struct rat n2){
	struct rat result;
	result.num = (n1.num * n2.den) + (n1.den * n2.num);
	result.den = n1.den * n2.den;

	return result;
}

struct rat rat_plus_petit(struct rat list[]){
	struct rat min;
	if (list[0].den == 0)
	{
		min.num = 1;
		min.den = 0;
		return min;
	}
	min.num = list[0].num;
	min.den = list[0].den;
	int i = 0;
	while(list[i].den != 0){
		if (list[i].den > min.den){
			if (list[i].num <= min.num)
				{
					min.num = list[i].num;
					min.den = list[i].den;
				}
		}else if(list[i].den == min.den){
			if (list[i].num < min.num)
				{
					min.num = list[i].num;
					min.den = list[i].den;
				}
		}else{
			if (list[i].num < min.num)
				{
					min.num = list[i].num;
					min.den = list[i].den;
				}
		}
		i++;
	}

	return min;
}

struct rat rat_simplifie(struct rat r){
	struct rat simpl;
	int tempDen = r.den;
	int tempNum = r.num;
	while(tempDen != 0){
		int temp = tempNum % tempDen;
		tempNum = tempDen;
		tempDen = temp;
	}
	simpl.num = r.num/tempNum;
	simpl.den = r.den/tempNum;
	if(simpl.den<0){
		simpl.num=-simpl.num;
		simpl.den=-simpl.den;
	}
	return simpl;
}