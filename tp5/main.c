#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include "functions.h" 

int main(int argc, char const *argv[])
{
	int *mots, mot;
	int *lignes, lig;
	int *caracs,car;
    mots = &mot;
    lignes = &lig;
    caracs = &car;
    int totaligne = 0;
    int param_w = 0;
    int param_l = 0;
	int param_c = 0;

    for (int i = 1; i < argc; ++i){
		
		if (argv[i][0] == '-'){
			int j = 1;
			while(argv[i][j] != '\0'){
			{
				if (argv[i][j] == 'w')
				{
					param_w = 1;
				}else if(argv[i][j] == 'l'){
					param_l = 1;
				}else if(argv[i][j] == 'c'){
					param_c = 1;
				}else{
					erreur();
					return 1;
				}
			}
				j++;
			}

		}
		
	}
	if (argc > 1)
    {
		if (param_l == 1 && param_w ==0)
		{
			for (int i = 2; i < argc; ++i)
		    	{
		    		int f = open(argv[i], O_RDONLY);
		    		if(traiter(f,caracs,mots,lignes) != 1){
		    			printf("lig = %d\n",lig );
						printf("lignes : %d --> %s\n",lig,argv[i]);
						totaligne+=*lignes;
					}else{
						erreur();
						return 1;
					}
					close(f);
		    	}
		    	printf("nb de ligne total : %d\n", totaligne);
		}else if(param_l == 0 && param_w == 1){
			int f = open(argv[2], O_RDONLY);
	    		if(traiter(f,caracs,mots,lignes) != 1){
					printf("mots : %d --> %s\n", mot,argv[2]);
				}else{
					erreur();
					return 1;
				}
		}else if(param_l == 1 && param_w == 1){
	    	int f = open(argv[argc-1], O_RDONLY);
    		if(traiter(f,caracs,mots,lignes) != 1){
				printf("mots : %d lignes : %d --> %s\n", mot,lig,argv[argc-1]);
			}else{
				printf("erreur ici\n");
				erreur();
				return 1;
			}
		}else if (param_l == 0 && param_w == 0)
		{
			int f = open(argv[1], O_RDONLY);
    		if(traiter(f,caracs,mots,lignes) != 1){
				printf("Il y a : %d mot %d car %d lig \n", mot,car,lig);
			}else{
				erreur();
				return 1;
			}
		}else if (param_c == 1 && param_w == 0 && param_l == 0)
		{
			int f = open(argv[1], O_RDONLY);
    		if(traiter(f,caracs,mots,lignes) != 1){
				printf("Il y a %d de caracteres\n", car);
			}else{
				erreur();
				return 1;
			}
		}
		}else{
			if (param_l == 0 && param_w == 0)
			{
				if(traiter(1,caracs,mots,lignes) != 1){
				printf("Il y a : %d mot %d car %d lig \n", mot,car,lig);
				}else{
					erreur();
					return 1;
				}
			}else{
				erreur();
				return 1;
			}
		}

	return 0;
}