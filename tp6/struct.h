/* 
 Pour avoir des types d'une taille connue et fixe, 
 on utilise les types définis dans stdint.h
*/
#include <stdint.h>

typedef struct
{
      uint16_t signature;
      uint32_t taille_fichier;
      uint32_t reserve;
      uint32_t offset_donnees;
} entete_fichier;

typedef struct
{
      uint32_t taille_entete;
      uint32_t largeur;
      uint32_t hauteur;
      uint16_t nombre_plans;
      uint16_t profondeur;
      uint32_t compression;
      uint32_t taille_donnees_image;
      uint32_t resolution_horizontale;
      uint32_t resolution_verticale;
      uint32_t taille_palette; /* en nombre de couleurs */
      uint32_t nombre_de_couleurs_importantes; /* 0 */
} entete_bitmap;

typedef struct
{
      entete_fichier fichier;
      entete_bitmap bitmap;
} entete_bmp;