#include "struct.h"

int lire_deux_octets(int fd, uint16_t *val);
int lire_quatre_octets(int fd, uint32_t *val);
int lire_entete(int de, entete_bmp *entete);
int ecrire_deux_octets(int fd, uint16_t val);
int ecrire_quatre_octets(int fd, uint32_t val);
int ecrire_entete(int vers, entete_bmp *entete);
int verifier_entete(entete_bmp *entete);
unsigned char* allouer_pixels(entete_bmp *entete);
int lire_pixels(int de, entete_bmp *entete, unsigned char *pixels);