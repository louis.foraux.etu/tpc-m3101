#include "parser.h"
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char const *argv[])
{
    if(argc != 2){
        printf("mauvaise utilisation");
        return 1;
    }
    int fd = open(argv[1], O_RDONLY);
    entete_bmp bmp;

    if(fd==-1){
        perror("erreur de l'open du fichier");
        return 1;
    }

    lire_entete(fd,&bmp);

    printf("fichier.offset_donnees : %d\n",bmp.fichier.offset_donnees);
    printf("fichier.reserve : %d\n",bmp.fichier.reserve);
    printf("fichier.signature : %d\n",bmp.fichier.signature);
    printf("fichier.taille_fichier : %d\n",bmp.fichier.taille_fichier);
    printf("bitmap.compression : %d\n",bmp.bitmap.compression);
    printf("bitmap.hauteur : %d\n",bmp.bitmap.hauteur);
    printf("bitmap.largeur : %d\n",bmp.bitmap.largeur);
    printf("bitmap.nb_col_import : %d\n",bmp.bitmap.nombre_de_couleurs_importantes);
    printf("bitmap.nbplans : %d\n",bmp.bitmap.nombre_plans);
    printf("bitmap.profondeur : %d\n",bmp.bitmap.profondeur);
    printf("bitmap.res_horizontale : %d\n",bmp.bitmap.resolution_horizontale);
    printf("bit.res_verticale : %d\n",bmp.bitmap.resolution_verticale);
    printf("bitmap.taille_donnees_image : %d\n",bmp.bitmap.taille_donnees_image);
    printf("bitmap.taille_entete : %d\n",bmp.bitmap.taille_entete);
    printf("bitmap.taile_palette : %d\n",bmp.bitmap.taille_palette);

    printf("bonne entete : %d\n",verifier_entete(&bmp));
    
    return 0;
}
