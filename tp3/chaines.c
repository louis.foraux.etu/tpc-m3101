#include <stdlib.h> 
#include <stdio.h> 
int mon_strlen_tab(char s[]){
	int compt = 0;
	while(s[compt] != '\0'){
		compt++; 
	}
	return compt;
}

int mon_strlen(const char *s){
	int compt = 0;
	while(s[compt] != '\0'){
		compt++;
	}
	return compt;
}

int mon_strcmp(const char * s1, const char * s2){
	int i = 0;
	while(s1[i] && s2[i] && s1[i] == s2[i]){
		i++;
	}
	return s1[i] - s2[i];
}

int mon_strncmp(const char * s1, const char * s2, int n){
	for (int i = 0; i < n; ++i){
		if (s1[i] != s2[i]){
			if (s1[i] > s2[i]){return 1;}
			else{return -1;}
		}
	}
	return 0;
}

char *mon_strcat(char *s1, const char *s2){
	int compt=0;
	int compt2=0;
	while(s1[compt]){compt++;}
	s1[compt] = s2[0];

	while(s2[compt2]){
		s1[compt] = s2[compt2];
		compt2++;
		compt++;
	}

	s1[compt] = '\0';

	return s1;
}

char *mon_strchr(const char *s, int c){
	int compt = 0;
	while(s[++compt]){
		if (s[compt] == (char)c)
		{
			return (char *)&s[compt];
		}
	}
	return NULL;
}

char *mon_strstr(const char *haystack, const char *needle){
	int drap = 0;
	if (mon_strlen(needle) == 0){return (char *)haystack;}
	char *s = mon_strchr(haystack,(int) needle[0]);
	while(s != NULL){
		drap = 0;
		for (int i = 0; i < mon_strlen(needle); ++i){
			//printf("%c et %c\n", s[i], needle[i]);
			if (s[i] != needle[i])
				{
					drap = 1;
				}	
		}
		if (drap == 1){
			s = mon_strchr(s,(int) needle[0]);
		}else{
			
			return s;
		}
	 
	}
	return NULL;
}