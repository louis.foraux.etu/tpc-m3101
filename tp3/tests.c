#include <stdio.h>
#include "chaines.h"
#include "munit.h"

#define UNUSED(x) (void)(x)


MunitResult test_strlen_tab(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  munit_assert_int(mon_strlen_tab(""), ==, 0);
  munit_assert_int(mon_strlen_tab("a"), ==, 1);
  char tab[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8};
  munit_assert_int(mon_strlen_tab(tab), ==, 9);
  
  return MUNIT_OK;
}

MunitResult test_strlen(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  munit_assert_int(mon_strlen(""), ==, 0);
  munit_assert_int(mon_strlen("a"), ==, 1);
  char tab[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8};
  munit_assert_int(mon_strlen(tab), ==, 9);
  
  return MUNIT_OK;
}

MunitResult test_strcmp(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  munit_assert_int(mon_strcmp("abcdef","abcdef"), ==,  0);
  munit_assert_int(mon_strcmp("abcdef","abcdefg"), <,  0);
  munit_assert_int(mon_strcmp("il fait moche aujourd'hui","il fait beau aujourd'hui"), >,  0);
  munit_assert_int(mon_strcmp("","abcdefg"), <,  0);
  munit_assert_int(mon_strcmp("zerzer",""), >,  0);
  munit_assert_int(mon_strcmp("",""), ==,  0);
  
  return MUNIT_OK;
}

MunitResult test_strncmp(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  munit_assert_int(mon_strncmp("abcdef","abcdef", 30), ==,  0);
  munit_assert_int(mon_strncmp("abcdef","abcdefg", 30), <,  0);
  munit_assert_int(mon_strncmp("il fait moche aujourd'hui","il fait beau aujourd'hui", 30), >,  0);
  munit_assert_int(mon_strncmp("","abcdefg", 30), <,  0);
  munit_assert_int(mon_strncmp("zerzer","", 30), >,  0);
  munit_assert_int(mon_strncmp("","", 30), ==,  0);


  munit_assert_int(mon_strncmp("abcde","abcde", 5), ==,  0);
  munit_assert_int(mon_strncmp("abcde","abcde", 4), ==,  0);
  munit_assert_int(mon_strncmp("abcde","abcde", 0), ==,  0);


  munit_assert_int(mon_strncmp("abcde","abcdeazeaze", 5), ==,  0);
  munit_assert_int(mon_strncmp("abcee","abcde", 4), >,  0);
  munit_assert_int(mon_strncmp("abcde","abcee", 4), <,  0);
  munit_assert_int(mon_strncmp("abcde","vbcde", 0), ==,  0);

  munit_assert_int(mon_strncmp("","", 0), ==,  0);
  return MUNIT_OK;
}

MunitResult test_strcat(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  char c1[40] = "Bonjour";
  char c2[] = " le monde";
  char * r = mon_strcat(c1, c2);
  munit_assert_ptr_equal(c1, r);
  munit_assert_string_equal(r, "Bonjour le monde");

  char bt[] = {-100, 'f', 'g', 'h', '\0', 6, 7, 8, 9, 10, -110};
  char expected[] = {-100, 'f', 'g', 'h', 'a', 'b', 'c', '\0', 9, 10, -110};
  int s = sizeof(expected)/sizeof(expected[0]);
  mon_strcat(bt, "abc");
  for (int i = 0 ; i < s ; ++i) {
    munit_assert_char(bt[i], ==, expected[i]);
  }
  return MUNIT_OK;
}


MunitResult test_strchr(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  munit_assert_ptr_null(mon_strchr("Bonjour", 'z'));
  munit_assert_ptr_null(mon_strchr("", 'z'));
  /* munit_assert_ptr_null(mon_strchr("zer", '\0')); */
  /* munit_assert_ptr_null(mon_strchr("", '\0')); */
  const char *s = "Bonjour le monde";
  munit_assert_ptr_equal(mon_strchr(s, 'o'), strchr(s, 'o'));

  const char t[] = {'a', 'b', 'c', '\0', 'd'};
  munit_assert_ptr_null(mon_strchr(t, 'd'));
  
  return MUNIT_OK;
}


MunitResult test_strstr(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  const char *s = "Bonjour le monde";
  const char *expected = strstr(s, "ond");
  munit_assert_ptr_equal(mon_strstr(s, "ond"), expected);

  munit_assert_ptr_null(mon_strstr(s, "Hop"));
  munit_assert_ptr_equal(mon_strstr(s, ""), s);
  const char *empty = "";
  munit_assert_ptr_equal(mon_strstr(empty, empty), empty);
  munit_assert_ptr_null(mon_strstr("", "hop"));
  
  return MUNIT_OK;
}

// ajouter strcasecmp et strcasencmp

MunitTest tests[] = {
    {"/strlen", test_strlen_tab, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/strlen", test_strlen, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/strcmp", test_strcmp, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/strncmp", test_strncmp, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/strcat", test_strcat, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/strchr", test_strchr, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    {"/strstr", test_strstr, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    /* Mark the end of the array with an entry where the test
     * function is NULL */
    {NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}};

static const MunitSuite suite = {
    "/chaines",       /* name */
    tests,          /* tests */
    NULL,                   /* suites */
    300,                    /* iterations */
    MUNIT_SUITE_OPTION_NONE /* options */
};

int main(int argc, char **argv) {

  int ret = munit_suite_main(&suite, NULL, argc, argv);
  return ret;
}
